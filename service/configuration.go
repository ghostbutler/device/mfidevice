package service

import (
	"encoding/json"
	"io/ioutil"
)

type Configuration struct {
	// security manager
	SecurityManager struct {
		Hostname string `json:"hostname"`
		Username string `json:"username"`
		Password string `json:"password"`
	} `json:"securitymanager"`

	// directory
	Directory struct {
		Hostname string `json:"hostname"`
	} `json:"directory"`

	// scanner
	Scanner struct {
		Hostname string `json:"hostname"`
	} `json:"scanner"`

	// mfi
	MFI struct {
		Login    string `json:"login"`
		Password string `json:"password"`
	}

	// rabbit mq
	RabbitMQ struct {
		Hostname []string `json:"hostname"`
		Username string   `json:"username"`
		Password string   `json:"password"`
	}
}

// build configuration
func BuildConfiguration(configurationFilePath string) (*Configuration, error) {
	// read configuration file
	if content, err := ioutil.ReadFile(configurationFilePath); err == nil {
		// configuration instance
		var configuration Configuration

		// parse json
		if err := json.Unmarshal(content,
			&configuration); err == nil {
			return &configuration, nil
		} else {
			return nil, err
		}
	} else {
		return nil, err
	}
}
