package service

import (
	"encoding/json"
	"gitlab.com/ghostbutler/tool/service/device"
)

type Outlet struct {
	Port        int     `json:"port"`
	ID          string  `json:"id"`
	Name        string  `json:"label"`
	Model       string  `json:"model"`
	Output      int     `json:"output"`
	Power       float64 `json:"power"`
	Energy      float64 `json:"energy"`
	Enabled     int     `json:"enabled"`
	Current     float64 `json:"current"`
	Voltage     float64 `json:"voltage"`
	PowerFactor float64 `json:"powerfactor"`
	Relay       int     `json:"relay"`
	Lock        int     `json:"lock"`

	IsMustSave bool `json:"-"`
}

type OutletNewFormat struct {
	Port  int    `json:"port"`
	ID    string `json:"id"`
	Name  string `json:"label"`
	Model string `json:"model"`
	State struct {
		Output      int     `json:"output"`
		Power       float64 `json:"power"`
		Energy      float64 `json:"energy"`
		Enabled     int     `json:"enabled"`
		Current     float64 `json:"current"`
		Voltage     float64 `json:"voltage"`
		PowerFactor float64 `json:"powerfactor"`
		Relay       int     `json:"relay"`
		Lock        int     `json:"lock"`
	}
}

func (outlet *Outlet) IsDifferent(oldOutlet *Outlet) bool {
	return outlet.Name != oldOutlet.Name ||
		outlet.Current != oldOutlet.Current ||
		outlet.Enabled != oldOutlet.Enabled ||
		outlet.Energy != oldOutlet.Energy ||
		outlet.Lock != oldOutlet.Lock ||
		outlet.Output != oldOutlet.Output ||
		outlet.Power != oldOutlet.Power ||
		outlet.PowerFactor != oldOutlet.PowerFactor ||
		outlet.Voltage != oldOutlet.Voltage
}

func (outlet *Outlet) MarshalJson(controllerAddress string) []byte {
	content, _ := json.Marshal(&device.Outlet{
		IP:          controllerAddress,
		Name:        outlet.Name,
		Current:     outlet.Current,
		Enabled:     outlet.Enabled,
		Energy:      outlet.Energy,
		ID:          outlet.ID,
		Output:      outlet.Output,
		Port:        outlet.Port,
		Power:       outlet.Power,
		PowerFactor: outlet.PowerFactor,
		Voltage:     outlet.Voltage,
	})
	return device.DoExport(device.CapabilityTypeOutlet,
		content)
}

type OutletList struct {
	Outlet []Outlet `json:"sensors"`
}

type OutletListNewFormat struct {
	Outlet []OutletNewFormat `json:"sensors"`
}

func (outletList *OutletList) FindOutlet(id string) *Outlet {
	for _, outlet := range outletList.Outlet {
		if outlet.ID == id {
			return &outlet
		}
	}
	return nil
}
