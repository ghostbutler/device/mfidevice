package service

import (
	"bytes"
	"crypto/tls"
	"encoding/json"
	"fmt"
	"github.com/streadway/amqp"
	"gitlab.com/ghostbutler/tool/service"
	"gitlab.com/ghostbutler/tool/service/rabbit"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"
	"sync"
	"time"
)

const (
	// session id associated to the MFI login
	MFICookieSessionID = "AIROS_SESSIONID=01234567890123456789012345678901"

	// delay between two mfi status update
	DelayBetweenMFIUpdate = time.Millisecond * 500

	// delay before an unconnected mfi is discarded
	DiscardMFITime = time.Second * 20

	// request timeout
	MFIRequestTimeout = time.Second * 15
)

type Controller struct {
	// rabbit mq controller
	rabbitmqController *rabbit.Controller

	// is update thread running
	isRunning bool

	// address
	address string

	// outlet List
	outletList OutletList

	// is connected to outlet
	isConnectedToOutlet bool

	// last connection time
	lastConnectionTime time.Time

	// mfi login
	username, password string

	// http client
	httpClient *http.Client

	// test
	testMutex sync.Mutex
	test      map[int]bool
}

// build controller
func BuildController(address string,
	login string,
	password string,
	rabbitmqController *rabbit.Controller) *Controller {
	output := &Controller{
		isRunning:           true,
		isConnectedToOutlet: false,
		address:             address,
		lastConnectionTime:  time.Now(),
		username:            login,
		password:            password,
		rabbitmqController:  rabbitmqController,
		test:                make(map[int]bool),
	}
	output.httpClient = &http.Client{
		Transport: &http.Transport{
			TLSClientConfig: &tls.Config{
				InsecureSkipVerify: true,
			},
		},
		CheckRedirect: func(req *http.Request, via []*http.Request) error {
			return http.ErrUseLastResponse
		},
		Timeout: MFIRequestTimeout,
	}
	go output.updateThread()
	return output
}

// update an outlet
func (controller *Controller) updateOutlet() bool {
	// build request
	request, _ := http.NewRequest("GET",
		"https://"+
			controller.address+
			"/sensors",
		nil)

	// add session id
	request.Header.Add("Cookie",
		MFICookieSessionID)

	// send request
	if response, err := controller.httpClient.Do(request); err == nil {
		body, _ := ioutil.ReadAll(response.Body)
		_ = response.Body.Close()
		var outletList OutletList
		if response.StatusCode == http.StatusOK {
			if strings.Contains(string(body),
				"\"state\":") {
				var outletListNewFormat OutletListNewFormat
				if err := json.Unmarshal(body,
					&outletListNewFormat); err == nil {
					outletList.Outlet = make([]Outlet,
						len(outletListNewFormat.Outlet))
					for index, outlet := range outletListNewFormat.Outlet {
						outletList.Outlet[index] = Outlet{
							Name:        outlet.Name,
							ID:          outlet.ID,
							Model:       outlet.Model,
							Port:        outlet.Port,
							Power:       outlet.State.Power,
							Current:     outlet.State.Current,
							Enabled:     outlet.State.Enabled,
							Energy:      outlet.State.Energy,
							Lock:        outlet.State.Lock,
							Output:      outlet.State.Output,
							PowerFactor: outlet.State.PowerFactor,
							Relay:       outlet.State.Relay,
							Voltage:     outlet.State.Voltage,
						}
					}
				} else {
					return false
				}
			} else {
				if err := json.Unmarshal(body,
					&outletList); err != nil {
					// disconnect mfi
					return false
				}
			}
			for _, outlet := range outletList.Outlet {
				if oldOutlet := controller.outletList.FindOutlet(outlet.ID); oldOutlet == nil {
					outlet.IsMustSave = true
				} else {
					if outlet.IsDifferent(oldOutlet) {
						outlet.IsMustSave = true
					}
				}
				if outlet.IsMustSave {
					if channel := controller.rabbitmqController.Channel; channel != nil {
						_ = channel.Publish("",
							rabbit.SensorQueueName,
							false,
							false,
							amqp.Publishing{
								ContentType: "application/json",
								Body:        outlet.MarshalJson(controller.address),
							})
					}
				}
			}
			controller.outletList = outletList
			return true
		} else {
			// disconnect mfi
			return false
		}
	} else {
		// disconnect mfi
		return false
	}
}

func (controller *Controller) login(username string, password string) bool {
	// build request
	request, _ := http.NewRequest("POST",
		"https://"+
			controller.address+
			"/login.cgi",
		bytes.NewReader([]byte("username="+
			username+
			"&password="+
			password)))

	// add session id
	request.Header.Add("Cookie",
		MFICookieSessionID)

	// add content type
	request.Header.Add("Content-Type",
		"application/x-www-form-urlencoded")

	// do request
	if response, err := controller.httpClient.Do(request); err == nil {
		// defer close
		defer common.Close(response.Body)

		// discard body
		d, _ := ioutil.ReadAll(response.Body)

		// check response
		if (response.StatusCode == http.StatusOK ||
			response.StatusCode == http.StatusFound) &&
			len(d) == 0 {
			// login succeeded
			return true
		} else {
			// login failed
			return false
		}
	} else {
		// couldn't send request
		return false
	}
}

// enable/disable outlet by its name
func (controller *Controller) SetEnable(outletName string,
	isOn bool) {
	if controller.outletList.Outlet != nil {
		for _, outlet := range controller.outletList.Outlet {
			if outlet.Name == outletName {
				controller.setEnable(outlet.Port,
					isOn)
			}
		}
	}
}

// enable/disable all outlets
func (controller *Controller) SetEnableAll(isOn bool) {
	if controller.outletList.Outlet != nil {
		for _, outlet := range controller.outletList.Outlet {
			controller.setEnable(outlet.Port,
				isOn)
		}
	}
}

// enable/disable outlet by its index
func (controller *Controller) setEnable(outlet int, isOn bool) {
	// bool map
	BoolMap := map[bool]string{
		true:  "1",
		false: "0",
	}

	// build request
	request, _ := http.NewRequest("PUT",
		"https://"+
			controller.address+
			"/sensors/"+
			strconv.Itoa(outlet)+
			"/output",
		bytes.NewReader([]byte("output="+BoolMap[isOn])))

	// add session id cookie
	request.Header.Add("Cookie",
		MFICookieSessionID)

	// add content type
	request.Header.Add("Content-Type",
		"application/x-www-form-urlencoded")

	// discard response
	common.DiscardHTTPResponse(controller.httpClient.Do(request))
}

// test outlet
func (controller *Controller) Test(outletName string) {
	if controller.outletList.Outlet != nil {
		for _, outlet := range controller.outletList.Outlet {
			if outlet.Name == outletName {
				isMustTest := true
				controller.testMutex.Lock()
				if isTest, ok := controller.test[outlet.Port]; ok {
					if isTest {
						isMustTest = false
					}
				} else {
					controller.test[outlet.Port] = true
				}
				controller.testMutex.Unlock()

				if isMustTest {
					if outlet.Output == 1 {
						controller.setEnable(outlet.Port,
							false)
						time.Sleep(time.Second * 3)
						controller.setEnable(outlet.Port,
							true)
					} else {
						controller.setEnable(outlet.Port,
							true)
						time.Sleep(time.Second * 3)
						controller.setEnable(outlet.Port,
							false)
					}

					controller.testMutex.Lock()
					controller.test[outlet.Port] = false
					controller.testMutex.Unlock()
				}
			}
		}
	}
}

// update thread
func (controller *Controller) updateThread() {
	// is controller running?
	for controller.isRunning {
		// are we connected to the outlet?
		if controller.isConnectedToOutlet {
			// has update succeeded?
			if !controller.updateOutlet() {
				// we are no longer connected to the outlet
				controller.isConnectedToOutlet = false

				// notify
				fmt.Println("couldn't update outlet at",
					controller.address,
					", now disconnected")

				// save last connection time
				controller.lastConnectionTime = time.Now()
			}
		} else {
			// try to connect to the outlet
			if controller.isConnectedToOutlet = controller.login(controller.username,
				controller.password); !controller.isConnectedToOutlet {
				fmt.Println("connection to outlet",
					controller.address,
					"failed...")
				// time out?
				if time.Now().Sub(controller.lastConnectionTime) >= DiscardMFITime {
					// stop controller
					controller.isRunning = false

					// notify
					fmt.Println("controller for outlet",
						controller.address,
						"is now discarded")
				}
			}
		}

		// sleep
		time.Sleep(DelayBetweenMFIUpdate)
	}
}
