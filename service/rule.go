package service

const (
	DetectionRule =
`{
	"rule": {
		"name": "mfi",
		"port": 80,
		"path": "/mfi/ping.cgi",
		"method": "GET",
		"isHTTPS": false,
		"data": "",
		"possibleAnswer": [
			{
				"responseCode": 200,
				"isMustAnalyzeContent": true,
				"validContent": [
					{
						"expectedValue": "eyJyc3AiOiJhY2sifQ==",
						"isMustBeEqual": false
					}
				]
			},
			{
				"responseCode": 200,
				"isMustAnalyzeContent": true,
				"validContent": [
					{
						"expectedValue": "eyJzdGF0dXMiOiJzdWNjZXNzIn0=",
						"isMustBeEqual": false
					}
				]
			},
			{
				"responseCode": 302,
				"isMustAnalyzeHeader": true,
				"validHeader": [
					{
						"name": "Set-Cookie",
						"expectedValue": "QUlST1NfU0VTU0lPTklEPQ==",
						"isMustBeEqual": false
					},
					{
						"name": "Location",
						"expectedValue": "L2Nvb2tpZWNoZWNrZXI/dXJpPS9tZmkvcGluZy5jZ2k=",
						"isMustBeEqual": true
					}
				]
			}
		]
	},
	"controller": {
		"port": 16562,
		"path": "/api/v1/controller/add"
	}
}`
)
