package service

import (
	"encoding/json"
	"fmt"
	"gitlab.com/ghostbutler/tool/service"
	"gitlab.com/ghostbutler/tool/service/device"
	"io/ioutil"
	"net/http"
)

const (
	APIServiceV1MFIControllerAddDevice = common.APIServiceType(iota + common.APIServiceBuiltInLast)
	APIServiceV1MFIControllerOutletOn
	APIServiceV1MFIControllerOutletOff
	APIServiceV1MFIControllerOutlets
	APIServiceV1MFIControllerOutletsListName
	APIServiceV1MFIControllerOutletsTest
)

var APIService = map[common.APIServiceType]*common.APIEndpoint{
	APIServiceV1MFIControllerAddDevice: {
		Path:                    []string{"api", "v1", "controller", "add"},
		Method:                  "POST",
		Description:             "Add a new found device to MFI controller",
		Callback:                controllerAddNewDeviceCallback,
		IsMustProvideOneTimeKey: true,
	},
	APIServiceV1MFIControllerOutletOn: {
		Path:                    []string{"api", "v1", "controller", "outlets", "on"},
		Method:                  "PUT",
		Description:             "Enable outlet by its name passed in post data ([name], if nothing, on all outlets)",
		Callback:                controllerOutletOn,
		IsMustProvideOneTimeKey: true,
	},
	APIServiceV1MFIControllerOutletOff: {
		Path:                    []string{"api", "v1", "controller", "outlets", "off"},
		Method:                  "PUT",
		Description:             "Disable outlet by its name passed in post data ([name], if nothing, off all outlets)",
		Callback:                controllerOutletOff,
		IsMustProvideOneTimeKey: true,
	},
	APIServiceV1MFIControllerOutlets: {
		Path:                    []string{"api", "v1", "controller", "outlets"},
		Method:                  "GET",
		Description:             "Give a complete list of known outlets with their state",
		Callback:                controllerGetOutletsList,
		IsMustProvideOneTimeKey: true,
	},
	APIServiceV1MFIControllerOutletsListName: {
		Path:                    []string{"api", "v1", "controller", "outlets", "list"},
		Method:                  "GET",
		Description:             "Give a complete list of controlled outlets names",
		Callback:                controllerGetOutletsListName,
		IsMustProvideOneTimeKey: true,
	},
	APIServiceV1MFIControllerOutletsTest: {
		Path:                    []string{"api", "v1", "controller", "outlets", "test"},
		Method:                  "POST",
		Description:             "Test an outlet (name)",
		Callback:                controllerTestOutlet,
		IsMustProvideOneTimeKey: true,
	},
}

// add new device to list
func controllerAddNewDeviceCallback(rw http.ResponseWriter,
	request *http.Request,
	_ *common.Service,
	handle interface{}) int {
	// get service
	srv := handle.(*Service)

	// check key
	if body, err := ioutil.ReadAll(request.Body); err == nil {
		var resultList common.ScannerResultList
		if err := json.Unmarshal(body,
			&resultList); err == nil {
			for _, result := range resultList.Result {
				if _, ok := srv.controller[result.IP]; !ok {
					srv.Lock()
					srv.controller[result.IP] = BuildController(result.IP,
						srv.configuration.MFI.Login,
						srv.configuration.MFI.Password,
						srv.rabbitController)
					fmt.Println("just added new controller for mfi at",
						result.IP)
					srv.Unlock()
				}
			}
			return http.StatusOK
		} else {
			rw.WriteHeader(http.StatusBadRequest)
			_, _ = rw.Write([]byte("{\"error\":\"invalid json\"}"))
			return http.StatusBadRequest
		}
	} else {
		rw.WriteHeader(http.StatusBadRequest)
		_, _ = rw.Write([]byte("{\"error\":\"can't read body\"}"))
		return http.StatusBadRequest
	}
}

// get outlets list
func controllerGetOutletsList(rw http.ResponseWriter,
	_ *http.Request,
	_ *common.Service,
	handle interface{}) int {
	// get service
	srv := handle.(*Service)

	// allocate outlet list
	outletList := make([]*device.Outlet,
		0,
		1)

	// lock
	srv.Lock()

	// iterate controllers
	for _, controller := range srv.controller {
		for _, outlet := range controller.outletList.Outlet {
			outletList = append(outletList,
				&device.Outlet{
					IP:          controller.address,
					Name:        outlet.Name,
					Current:     outlet.Current,
					Enabled:     outlet.Enabled,
					Energy:      outlet.Energy,
					ID:          outlet.ID,
					Output:      outlet.Output,
					Port:        outlet.Port,
					Power:       outlet.Power,
					PowerFactor: outlet.PowerFactor,
					Voltage:     outlet.Voltage,
				})
		}
	}

	// unlock
	srv.Unlock()

	// marshal data
	data, _ := json.MarshalIndent(&outletList,
		"",
		"\t")

	// send data
	_, _ = rw.Write(data)
	return http.StatusOK
}

// get outlets name list
func controllerGetOutletsListName(rw http.ResponseWriter,
	_ *http.Request,
	_ *common.Service,
	handle interface{}) int {
	// get service
	srv := handle.(*Service)

	// allocate outlet list
	outletList := make([]string,
		0,
		1)

	// lock
	srv.Lock()

	// iterate controllers
	for _, controller := range srv.controller {
		for _, outlet := range controller.outletList.Outlet {
			outletList = append(outletList,
				outlet.Name)
		}
	}

	// unlock
	srv.Unlock()

	// marshal data
	data, _ := json.MarshalIndent(&outletList,
		"",
		"\t")

	// send data
	_, _ = rw.Write(data)
	return http.StatusOK
}

// outlet off
func controllerOutletOff(rw http.ResponseWriter,
	request *http.Request,
	_ *common.Service,
	handle interface{}) int {
	srv := handle.(*Service)
	if err := request.ParseForm(); err == nil {
		if values := request.Form["name"]; values != nil &&
			len(values) > 0 &&
			len(values[0]) > 0 {
			srv.SetEnable(values[0],
				false)
		} else {
			srv.SetEnableAll(false)
		}
		return http.StatusOK
	} else {
		rw.WriteHeader(http.StatusBadRequest)
		_, _ = rw.Write([]byte("{\"error\":\"bad form\"}"))
		return http.StatusBadRequest
	}
}

// outlet on
func controllerOutletOn(rw http.ResponseWriter,
	request *http.Request,
	_ *common.Service,
	handle interface{}) int {
	srv := handle.(*Service)
	if err := request.ParseForm(); err == nil {
		if values := request.Form["name"]; values != nil &&
			len(values) > 0 &&
			len(values[0]) > 0 {
			srv.SetEnable(values[0],
				true)
		} else {
			srv.SetEnableAll(true)
		}
		return http.StatusOK
	} else {
		rw.WriteHeader(http.StatusBadRequest)
		_, _ = rw.Write([]byte("{\"error\":\"bad form\"}"))
		return http.StatusBadRequest
	}
}

func controllerTestOutlet(rw http.ResponseWriter,
	request *http.Request,
	_ *common.Service,
	handle interface{}) int {
	srv := handle.(*Service)
	if err := request.ParseForm(); err == nil {
		if values := request.Form["name"]; values != nil &&
			len(values) > 0 &&
			len(values[0]) > 0 {
			go srv.Test(values[0])
		}
		return http.StatusOK
	} else {
		rw.WriteHeader(http.StatusBadRequest)
		_, _ = rw.Write([]byte("{\"error\":\"bad form\"}"))
		return http.StatusBadRequest
	}
}
