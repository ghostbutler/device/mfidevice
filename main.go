package main

import (
	"./service"
	"fmt"
	"gitlab.com/ghostbutler/tool/service"
	"os"
	"time"
)

const (
	DefaultConfigurationFilePath = "conf.json"
)

func main() {
	// configuration file path
	configurationFilePath := DefaultConfigurationFilePath
	if len(os.Args) > 1 {
		configurationFilePath = os.Args[1]
	}

	// load configuration
	if configuration, err := service.BuildConfiguration(configurationFilePath); err == nil {
		// build service
		if srv, err := service.BuildService(common.GhostService[common.ServiceControllerMFI].DefaultPort,
			configuration); err == nil {
			for {
				// update service
				srv.Update()

				// wait
				time.Sleep(time.Millisecond * 1000)
			}
		} else {
			fmt.Println("failed to build service:",
				err)
			os.Exit(1)
		}
	} else {
		// notify
		fmt.Println("couldn't load",
			configurationFilePath)

		// exit
		os.Exit(1)
	}
}
