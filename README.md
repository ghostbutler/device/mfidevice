MFI controller
==============

Introduction
------------

This service is dedicated to the control of the mfi outlets.

Configuration
-------------

```json
{
	"securityManager": {
		"hostname": "securitymanager.ghostbutler",
		"username": "GhostButler2018Scanner",
		"password": "GhostButlerProject2018*"
	},
	"scanner": {
		"hostname": "scanner.ghostbutler"
	},
	"mfi": {
		"login": "ghost",
		"password": "ghost"
	}
}
```

|name|type|description|
|----|----|-----------|
|`securitymanager.hostname`|string|the hostname of the security manager|
|`securitymanager.username`|string|the username to give to authenticate with security manager|
|`securitymanager.password`|string|the password associated to username to give to authenticate to the security manager|
|`scanner.hostname`|string|the hostname of the scanner|
|`mfi.login`|string|the login for mfi outlets|
|`mfi.password`|string|the password for mfi outlets|

Notes
-----

If you run the program as

```
./mfi configuration.json
```

you'll then specify which configuration to load

Outlet configuration
--------------------

To configure an outlet, you need a physical mFi hub. As this device does not exist anymore, you'll need to emulate it via
a docker image running same software the hub was.

The image can be found [here](https://hub.docker.com/r/fflo/mfi-controller/).

To run the image, execute this

```bash
docker run -v /tmp/mfi:/var/lib/mfi -p 3478:3478/udp -p 6080:6080 -p 6443:6443 fflo/mfi-controller
```

When run, the image output should say something like "comet awaiting message" or something like that (todo: edit to correct output)

Once that's done, connect to the image via http://127.0.0.1:6080/, and set a username/password, the same that will be used for the
outlets (by default and in mFi controllers, values are *ghost*/*ghost*)

The hub configuration is done. Now hold the reset button on the outlet, and wait for light bar to rapidly blink between blue and orange.

The outlet, once reset, will broadcast a wifi hotspot with a SSID looking as "MFIXXXX" (todo, correct this, can't remember). Connect to
the hotspot, and go to http://www.google.fr/ to be redirected to the outlet captive portal (the usage of http is better to be redirected
quicker).

From the portal, choose the wifi network your computer running mFi hub is connected to, and type the wifi password. Then on the bottom of
the page, type mFi hub emulated ip address (http://YOUR_IP:6080), and the username/password choosed previously (ghost/ghost in this example).

Once OK clicked, process will take one minute to be able to see the outlet in the web interface on the hub.

Last part of the process is to go into devices section in the mFi hub interface, and to give a name to each outlet for it to be activated.
(todo, complete this part, can't remember)

That's it, the outlet is configured, and will connect automatically to the wifi when plugged. You can shutdown the hub docker image, the outlet
does only need it to boot first time, later this is useless.

The outlets will then be found by the scanner service via the rule set into `service/rule.go`.

Test
----

To test outlet on function

```bash
curl -k -H "X-Ghost-Butler-Key: `accessgenerator "127.0.0.1" "GhostButler2018Admin" "GhostButlerProject2018*"`" https://127.0.0.1:16562/api/v1/controller/outlets/on -X PUT -d "name=Prise11"
```

Author
------

DAHMANI Chakir <dahmani.chakir@hotmail.fr>

https://gitlab.com/ghostbutler/device/mfidevice.git
